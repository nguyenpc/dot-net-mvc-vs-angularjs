﻿app.controller('CategoriesController', function ($scope, $http, $routeParams) {

    $http.get("/api/categories")
        .success(function (data) {
            $scope.categories = data
        })
})

app.controller('CategoryDetailController', function ($scope, $http, $routeParams) {

    var id = $routeParams.id

    $http.get("/api/categories/" + id)
    .success(function (data) {
        $scope.category = data
    })
}) 

app.controller('CategoryEditController', function ($scope, $http, $routeParams, $ApiService) {

    var id = $routeParams.id


    $http.get("/api/categories/" + id)
    .success(function (data) {
        $scope.category = data
    })
    
    $scope.save = function () {
        $ApiService.log($scope.category)
    }

})