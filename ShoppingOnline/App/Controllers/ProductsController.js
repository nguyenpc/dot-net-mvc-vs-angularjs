﻿app.controller('ProductsController', function ($scope, $http, $routeParams) {

    $http.get("/api/products")
        .success(function (data) {
            $scope.products = data
        })
})

app.controller('ProductDetailController', function ($scope, $http, $routeParams) {
    var id = $routeParams.id
    $http.get("/api/products/" + id)
        .success(function (data) {
            $scope.product = data
        })
})
