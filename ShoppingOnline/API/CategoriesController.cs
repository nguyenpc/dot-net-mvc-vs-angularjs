﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using Models;
using System.Web.Http.Description;

namespace ShoppingOnline.API
{
    public class CategoriesController : ApiController
    {
        private AppContext _db = new AppContext();

        // GET api/<controller>
        [ResponseType(typeof(List<Category>))]
        public IHttpActionResult Get()
        {
            var categories = new List<Category>();
            categories = _db.Categories.ToList();
            return Ok(categories);
        }

        // GET api/<controller>/5
        [ResponseType(typeof(Category))]
        public IHttpActionResult Get(int id)
        {
            var category = new Category();
            using (var context = new AppContext())
            {
                category = _db.Categories.Where(x => x.Id == id).Include(c => c.Products).FirstOrDefault();
            }
            return Ok(category);
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}